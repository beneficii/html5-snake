window.addEventListener("load", onWindowLoaded, false);
var canvas;
var rect = new fabric.Rect({
  left: 100,
  top: 100,
  fill: 'red',
  width: 20,
  height: 20
});



function onWindowLoaded(){
    canvas = new fabric.Canvas('snakeCanvas', {
    	renderOnAddition: false,
    	selection: false
  	});
    canvas.add(rect);
    //canvas.renderAll();
}